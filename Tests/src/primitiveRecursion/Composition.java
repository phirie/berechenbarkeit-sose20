package primitiveRecursion;

public class Composition extends PrimitiveRecursiveFunction {
    private PrimitiveRecursiveFunction compositionMainFunction;
    private PrimitiveRecursiveFunction[] compositionSubFunctions;

    public Composition(PrimitiveRecursiveFunction compositionMainFunction, PrimitiveRecursiveFunction... compositionSubFunctions) throws PrimitiveRecursionException{
        super(compositionSubFunctions[0].getDegree());
        checkIntegrity(compositionSubFunctions[0].getDegree(), compositionMainFunction, compositionSubFunctions);
        this.compositionMainFunction = compositionMainFunction;
        this.compositionSubFunctions = compositionSubFunctions;
    }

    private static void checkIntegrity(
            int degree,
            PrimitiveRecursiveFunction compositionMainFunction,
            PrimitiveRecursiveFunction... compositionSubFunctions)
            throws PrimitiveRecursionException {
        if (compositionMainFunction.getDegree() != compositionSubFunctions.length) {
            throw new PrimitiveRecursionException(
                    "Main-function degree("
                    + compositionMainFunction.getDegree()
                    + ") does not match number of given sub-functions("
                    + compositionSubFunctions.length
                    + ").");
        }

        for (PrimitiveRecursiveFunction subFunction : compositionSubFunctions) {
            if (subFunction.getDegree() != degree) {
                throw new PrimitiveRecursionException(
                        "Degree of given sub-function("
                        + subFunction
                        + ") does not match composition degree("
                        + degree
                        + ").");
            }
        }
    }

    @Override
    protected int calculateValue(int... parameters) throws PrimitiveRecursionException{
        int[] subResult = new int[compositionSubFunctions.length];
        for (int n = 0; n < compositionSubFunctions.length; n++) {
            subResult[n] = compositionSubFunctions[n].run(parameters);
        }

        return compositionMainFunction.run(subResult);
    }

    @Override
    public String generateLatexRepresentation() {
        StringBuilder result = new StringBuilder(compositionMainFunction.generateLatexRepresentation());

        result.append(" \\langle ");

        for (int n = 0; n < compositionSubFunctions.length - 1; n ++) {
            result.append(compositionSubFunctions[n].generateLatexRepresentation());
            result.append(", ");
        }
        result.append(compositionSubFunctions[compositionSubFunctions.length - 1].generateLatexRepresentation());

        result.append(" \\rangle ");

        return result.toString();
    }
}
