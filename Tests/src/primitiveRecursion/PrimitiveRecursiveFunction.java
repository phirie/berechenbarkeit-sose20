package primitiveRecursion;

public abstract class PrimitiveRecursiveFunction {
    public static final int MIN_DEGREE = 1;
    private int degree;

    public PrimitiveRecursiveFunction(int degree) throws PrimitiveRecursionException {
        if (degree < MIN_DEGREE) {
            throw new PrimitiveRecursionException("Degree can not be lower than: " + MIN_DEGREE);
        }
        this.degree = degree;
    }

    public int run(int... parameters) throws PrimitiveRecursionException {
        if (parameters.length != degree) {
            throw new PrimitiveRecursionException("The number of given parameters("
                    + parameters.length
                    + ") does not match the degree of the function("
                    + degree
                    +").");
        }

        return calculateValue(parameters);
    }

    protected abstract int calculateValue(int... parameters) throws PrimitiveRecursionException;

    public abstract String generateLatexRepresentation();

    public int getDegree() {
        return degree;
    }
}
