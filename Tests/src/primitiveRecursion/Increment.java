package primitiveRecursion;

public class Increment extends PrimitiveRecursiveFunction {
    public Increment() throws PrimitiveRecursionException{
        super(1);
    }

    @Override
    protected int calculateValue(int... parameters) {
        return parameters[0] + 1;
    }

    @Override
    public String generateLatexRepresentation() {
        return "nf";
    }
}
