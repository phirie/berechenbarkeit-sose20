package primitiveRecursion;

public class AdvancedFunctions {
    private static class Subtraction extends PrimitiveRecursiveFunction {
        public Subtraction() throws PrimitiveRecursionException {
            super(2);
        }

        @Override
        protected int calculateValue(int... parameters) throws PrimitiveRecursionException {
            return Math.max(0, parameters[0] - parameters[1]);
        }

        @Override
        public String generateLatexRepresentation() {
            return "sub";
        }
    }

    private static class Addition extends PrimitiveRecursiveFunction {
        public Addition() throws PrimitiveRecursionException {
            super(2);
        }

        @Override
        protected int calculateValue(int... parameters) throws PrimitiveRecursionException {
            return parameters[0] + parameters[1];
        }

        @Override
        public String generateLatexRepresentation() {
            return "add";
        }
    }

    private static class Multiplication extends PrimitiveRecursiveFunction {
        public Multiplication() throws PrimitiveRecursionException {
            super(2);
        }

        @Override
        protected int calculateValue(int... parameters) throws PrimitiveRecursionException {
            return parameters[0] * parameters[1];
        }

        @Override
        public String generateLatexRepresentation() {
            return "mult";
        }
    }

    private static class Decrement extends PrimitiveRecursiveFunction {
        public Decrement() throws PrimitiveRecursionException {
            super(1);
        }

        @Override
        protected int calculateValue(int... parameters) throws PrimitiveRecursionException {
            return Math.max(0, parameters[0] - 1);
        }

        @Override
        public String generateLatexRepresentation() {
            return "vg";
        }
    }
    
    public static PrimitiveRecursiveFunction pi1to1;
    public static PrimitiveRecursiveFunction pi2to1;
    public static PrimitiveRecursiveFunction pi2to2;
    public static PrimitiveRecursiveFunction pi3to1;
    public static PrimitiveRecursiveFunction pi3to2;
    public static PrimitiveRecursiveFunction pi3to3;
    public static PrimitiveRecursiveFunction pi4to1;
    public static PrimitiveRecursiveFunction pi4to2;
    public static PrimitiveRecursiveFunction pi4to3;
    public static PrimitiveRecursiveFunction pi4to4;

    public static PrimitiveRecursiveFunction subtraction;
    public static PrimitiveRecursiveFunction addition;
    public static PrimitiveRecursiveFunction multiplication;
    public static PrimitiveRecursiveFunction decrement;
    
    public static PrimitiveRecursiveFunction absOfnMinusM;

    //logical
    public static PrimitiveRecursiveFunction not;
    public static PrimitiveRecursiveFunction and;
    public static PrimitiveRecursiveFunction or;
    public static PrimitiveRecursiveFunction equalityTest;

    public static void init() {
        try {
            subtraction = new Subtraction();
            addition = new Addition();
            multiplication = new Multiplication();
            decrement = new Decrement();

            initProjections();
            initAbsOfnMinusM();
            initLogical();
        } catch (PrimitiveRecursionException e) {
            e.printStackTrace();
        }
    }

    private static void initProjections() throws PrimitiveRecursionException{
        pi1to1 = new Projection(1, 1);

        pi2to1 = new Projection(2, 1);
        pi2to2 = new Projection(2, 2);

        pi3to1 = new Projection(3, 1);
        pi3to2 = new Projection(3, 2);
        pi3to3 = new Projection(3, 3);

        pi4to1 = new Projection(4, 1);
        pi4to2 = new Projection(4, 2);
        pi4to3 = new Projection(4, 3);
        pi4to4 = new Projection(4, 4);
    }
    
    private static void initAbsOfnMinusM() throws PrimitiveRecursionException {
        PrimitiveRecursiveFunction zeroCase = new Composition(subtraction, pi2to2, pi2to1);
        PrimitiveRecursiveFunction otherCase = new Composition(subtraction, new Projection(4, 3), new Projection(4, 4));
        PrimitiveRecursiveFunction recursion = new PrimitiveRecursion(zeroCase, otherCase);
        absOfnMinusM = new Composition(recursion, subtraction, pi2to1, pi2to2);
    }

    private static void initLogical() throws PrimitiveRecursionException{
        PrimitiveRecursiveFunction notGate = new PrimitiveRecursion(new Constant(1, 1), new Constant(3, 0));
        not = new Composition(notGate, pi1to1, pi1to1);

        PrimitiveRecursiveFunction innerOrIf = new PrimitiveRecursion(new Constant(1, 0), new Constant(3, 1));
        PrimitiveRecursiveFunction innerOrTotal = new Composition(innerOrIf, pi1to1, pi1to1);
        or = new PrimitiveRecursion(innerOrTotal, new Constant(3, 1));

        PrimitiveRecursiveFunction innerAndIf = new PrimitiveRecursion(new Constant(1, 0), new Constant(3, 1));
        PrimitiveRecursiveFunction innerAndTotal = new Composition(innerAndIf, pi3to3, pi3to3);
        and = new PrimitiveRecursion(new Constant(1, 0), innerAndTotal);

        PrimitiveRecursiveFunction equalityCases = new PrimitiveRecursion(new Constant(1, 1), new Constant(3, 0));
        equalityTest = new Composition(equalityCases, absOfnMinusM, new Constant(2, 0));
    }

    public static PrimitiveRecursiveFunction getWrapperFunction(PrimitiveRecursiveFunction valueFunction, String latexRep) throws PrimitiveRecursionException {
        class WrapperFunction extends PrimitiveRecursiveFunction {
            WrapperFunction() throws PrimitiveRecursionException {
                super(valueFunction.getDegree());
            }


            @Override
            protected int calculateValue(int... parameters) throws PrimitiveRecursionException {
                return valueFunction.run(parameters);
            }

            @Override
            public String generateLatexRepresentation() {
                return latexRep;
            }
        }

        return new WrapperFunction();
    }
}
