package primitiveRecursion;

public class Minimisation extends PrimitiveRecursiveFunction {

    private PrimitiveRecursiveFunction checkFunction;

    public Minimisation(PrimitiveRecursiveFunction checkFunction) throws PrimitiveRecursionException{
        super(checkFunction.getDegree() - 1);
        this.checkFunction = checkFunction;
    }

    @Override
    protected int calculateValue(int... parameters) throws PrimitiveRecursionException {
        int[] newParameters = new int[parameters.length + 1];
        System.arraycopy(parameters, 0, newParameters, 1, parameters.length);
        newParameters[0] = 0;
        while(true) {
            if (checkFunction.run(newParameters) == 0) {
                return newParameters[0];
            }
            newParameters[0]++;
        }
    }

    @Override
    public String generateLatexRepresentation() {
        return "\\mu" + checkFunction.generateLatexRepresentation();
    }
}
