package primitiveRecursion;

public class PrimitiveRecursion extends PrimitiveRecursiveFunction {
    private PrimitiveRecursiveFunction zeroCaseFunction;
    private PrimitiveRecursiveFunction otherCaseFunction;

    public PrimitiveRecursion(PrimitiveRecursiveFunction zeroCaseFunction, PrimitiveRecursiveFunction otherCaseFunction) throws PrimitiveRecursionException {
        super(otherCaseFunction.getDegree() - 1);
        checkIntegrity(zeroCaseFunction, otherCaseFunction);
        this.zeroCaseFunction = zeroCaseFunction;
        this.otherCaseFunction = otherCaseFunction;
    }

    private static void checkIntegrity(PrimitiveRecursiveFunction zeroCaseFunction, PrimitiveRecursiveFunction otherCaseFunction) throws PrimitiveRecursionException {
        if (zeroCaseFunction.getDegree() != otherCaseFunction.getDegree() - 2) {
            throw new PrimitiveRecursionException("Degree of Zero-Case-Function("
                    + zeroCaseFunction.getDegree()
                    + ") does not comply with the expected value of \"Other-CaseFunction - 2\"("
                    + (otherCaseFunction.getDegree() - 2) + ").");
        }
    }

    @Override
    protected int calculateValue(int... parameters) throws PrimitiveRecursionException {
        if (parameters[0] == 0) {
            int[] newParameters = new int[parameters.length - 1];
            System.arraycopy(parameters, 1, newParameters, 0, parameters.length - 1);
            return zeroCaseFunction.run(newParameters);
        } else {
            int[] recursionParameters = new int[parameters.length];
            System.arraycopy(parameters, 0, recursionParameters, 0, parameters.length);
            recursionParameters[0]--;

            int[] newParameters = new int[parameters.length + 1];
            System.arraycopy(recursionParameters, 0, newParameters, 1, recursionParameters.length);
            newParameters[0] = this.run(recursionParameters);

            return otherCaseFunction.run(newParameters);
        }
    }

    @Override
    public String generateLatexRepresentation() {
        StringBuilder result = new StringBuilder("pr");

        result.append(" \\langle ");

        result.append(zeroCaseFunction.generateLatexRepresentation());
        result.append(", ");
        result.append(otherCaseFunction.generateLatexRepresentation());

        result.append(" \\rangle ");

        return result.toString();
    }
}
