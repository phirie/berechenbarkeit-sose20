package primitiveRecursion;

public class Projection extends PrimitiveRecursiveFunction {
    private int projectionIndex;

    /**
     *
     * @param degree
     * @param projectionIndex starting with index 1 for first element (not 0)
     * @throws PrimitiveRecursionException
     */
    public Projection(int degree, int projectionIndex) throws PrimitiveRecursionException{
        super(degree);
        this.projectionIndex = projectionIndex;
    }

    @Override
    protected int calculateValue(int... parameters) {
        return parameters[projectionIndex - 1];
    }

    @Override
    public String generateLatexRepresentation() {
        return "\\pi^{(" + getDegree() +")}_{" + projectionIndex + "}";
    }
}
