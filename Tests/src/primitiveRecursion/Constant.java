package primitiveRecursion;

public class Constant extends PrimitiveRecursiveFunction {
    private int value;

    public Constant(int degree, int value) throws PrimitiveRecursionException {
        super(degree);
        this.value = value;
    }

    @Override
    protected int calculateValue(int... parameters) {
        return value;
    }

    @Override
    public String generateLatexRepresentation() {
        return value + "^{(" + getDegree() + ")}";
    }
}
