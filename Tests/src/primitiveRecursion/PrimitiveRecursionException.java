package primitiveRecursion;

public class PrimitiveRecursionException extends Exception {
    public PrimitiveRecursionException(String message) {
        super(message);
    }
}
