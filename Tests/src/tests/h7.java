package tests;

import primitiveRecursion.AdvancedFunctions;
import primitiveRecursion.Composition;
import primitiveRecursion.Constant;
import primitiveRecursion.Increment;
import primitiveRecursion.PrimitiveRecursion;
import primitiveRecursion.PrimitiveRecursionException;
import primitiveRecursion.PrimitiveRecursiveFunction;
import primitiveRecursion.Projection;

public class h7 {
    public static void main(String[] args) {
        try {
            AdvancedFunctions.init();
            PrimitiveRecursiveFunction h71 = AdvancedFunctions.absOfnMinusM;
            PrimitiveRecursiveFunction or = AdvancedFunctions.or;
            PrimitiveRecursiveFunction mtest = mtest(h71);
            PrimitiveRecursiveFunction teilt = teilt(or, mtest);
            PrimitiveRecursiveFunction teilbar = teilbar(teilt, or);
            PrimitiveRecursiveFunction prim = prim(teilbar);

            System.out.println(prim.generateLatexRepresentation());

            for (int n = 0; n < 20; n++) {
                if (prim.run(n) != 1) {
                    System.out.print("\t");
                }
                System.out.println(n);
            }

        } catch (PrimitiveRecursionException e) {
            e.printStackTrace();
        }
    }

    private static PrimitiveRecursiveFunction prim(PrimitiveRecursiveFunction teilbar) throws PrimitiveRecursionException {
        PrimitiveRecursiveFunction pi1to1 = new Projection(1, 1);
        PrimitiveRecursiveFunction notIF = new PrimitiveRecursion(new Constant(1, 1), new Constant(3, 0));
        PrimitiveRecursiveFunction notGate = new Composition(notIF, pi1to1, pi1to1);

        PrimitiveRecursiveFunction teilbarTest = AdvancedFunctions.getWrapperFunction(teilbar, "teilbar_{pr}");

        return new Composition(notGate, teilbarTest);
    }

    private static PrimitiveRecursiveFunction teilbar(PrimitiveRecursiveFunction teilt, PrimitiveRecursiveFunction or) throws PrimitiveRecursionException {
        PrimitiveRecursiveFunction pi1to1 = new Projection(1, 1);
        PrimitiveRecursiveFunction pi3to1 = new Projection(3, 1);
        PrimitiveRecursiveFunction pi3to2 = new Projection(3, 2);
        PrimitiveRecursiveFunction pi3to3 = new Projection(3, 3);

        PrimitiveRecursiveFunction teiltTest = AdvancedFunctions.getWrapperFunction(teilt, "teilt_{pr}");
        PrimitiveRecursiveFunction orTest = AdvancedFunctions.getWrapperFunction(or, "or_{pr}");

        PrimitiveRecursiveFunction zeroCase = new Composition(teiltTest, new Constant(1, 2), pi1to1);

        PrimitiveRecursiveFunction pi3to2Plus2 = new Composition(new Increment(), new Composition(new Increment(), pi3to2));
        PrimitiveRecursiveFunction tester = new Composition(teiltTest, pi3to2Plus2, pi3to3);
        PrimitiveRecursiveFunction otherCase = new Composition(orTest, pi3to1, tester);

        PrimitiveRecursiveFunction main = new PrimitiveRecursion(zeroCase, otherCase);
        return new Composition(main, pi1to1, pi1to1);
    }

    private static PrimitiveRecursiveFunction teilt(PrimitiveRecursiveFunction or, PrimitiveRecursiveFunction mtest) throws PrimitiveRecursionException {
        PrimitiveRecursiveFunction pi2to1 = new Projection(2, 1);
        PrimitiveRecursiveFunction pi2to2 = new Projection(2, 2);
        PrimitiveRecursiveFunction pi4to1 = new Projection(4, 1);
        PrimitiveRecursiveFunction pi4to2 = new Projection(4, 2);
        PrimitiveRecursiveFunction pi4to3 = new Projection(4, 3);
        PrimitiveRecursiveFunction pi4to4 = new Projection(4, 4);

        PrimitiveRecursiveFunction orTest = AdvancedFunctions.getWrapperFunction(or, "or_{pr}");
        PrimitiveRecursiveFunction multTest = AdvancedFunctions.getWrapperFunction(mtest, "mtest_{pr}");

        PrimitiveRecursiveFunction zeroCase = new Composition(multTest, pi2to1, pi2to2, new Constant(2, 2));

        PrimitiveRecursiveFunction pi4to2Plus2 = new Composition(new Increment(), new Composition(new Increment(), pi4to2));
        PrimitiveRecursiveFunction tester = new Composition(multTest, pi4to3, pi4to4, pi4to2Plus2);
        PrimitiveRecursiveFunction otherCase = new Composition(orTest, pi4to1, tester);

        PrimitiveRecursiveFunction main = new PrimitiveRecursion(zeroCase, otherCase);
        return new Composition(main, pi2to2, pi2to1, pi2to2);
    }

    private static PrimitiveRecursiveFunction mtest(PrimitiveRecursiveFunction h71) throws PrimitiveRecursionException {
        PrimitiveRecursiveFunction pi1to1 = new Projection(1, 1);
        PrimitiveRecursiveFunction pi3to1 = new Projection(3, 1);
        PrimitiveRecursiveFunction pi3to2 = new Projection(3, 2);
        PrimitiveRecursiveFunction pi3to3 = new Projection(3, 3);
        PrimitiveRecursiveFunction mult = AdvancedFunctions.multiplication;

        PrimitiveRecursiveFunction multWithProject = new Composition(mult, pi3to1, pi3to3);
        PrimitiveRecursiveFunction invert = new PrimitiveRecursion(new Constant(1, 1), new Constant(3, 0));
        PrimitiveRecursiveFunction compo1 = new Composition(invert, pi1to1, pi1to1);
        PrimitiveRecursiveFunction compo2 = new Composition(compo1, h71);
        return new Composition(compo2, multWithProject, pi3to2);
    }

    private static PrimitiveRecursiveFunction or() throws PrimitiveRecursionException {
        PrimitiveRecursiveFunction pi1to1 = new Projection(1, 1);

        PrimitiveRecursiveFunction innerIf = new PrimitiveRecursion(new Constant(1, 0), new Constant(3, 1));
        PrimitiveRecursiveFunction innerTotal = new Composition(innerIf, pi1to1, pi1to1);

        return new PrimitiveRecursion(innerTotal, new Constant(3, 1));
    }
}
