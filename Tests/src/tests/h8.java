package tests;

import javax.swing.event.SwingPropertyChangeSupport;
import primitiveRecursion.AdvancedFunctions;
import primitiveRecursion.Composition;
import primitiveRecursion.Constant;
import primitiveRecursion.Increment;
import primitiveRecursion.Minimisation;
import primitiveRecursion.PrimitiveRecursion;
import primitiveRecursion.PrimitiveRecursionException;
import primitiveRecursion.PrimitiveRecursiveFunction;
import primitiveRecursion.Projection;

public class h8 {
    private static final int MAX_VALUE = 100;
    public static void main(String[] args) {
        try {
            AdvancedFunctions.init();
            DIVMain();
        } catch (PrimitiveRecursionException e) {
            e.printStackTrace();
        }
    }

    private static void DIVMain() throws PrimitiveRecursionException{
        PrimitiveRecursiveFunction primitive = h82(AdvancedFunctions.getWrapperFunction(remainder(), "remainder"));
        PrimitiveRecursiveFunction h82 = minimised(AdvancedFunctions.getWrapperFunction(primitive, "DIV_{pr}"));

        for (int x = 0; x <= MAX_VALUE; x++) {
            for (int y = 1; y <= MAX_VALUE; y++) {
                int functionResult = h82.run(x, y);
                System.out.println("(" + x + " / " + y + ") --> " + functionResult);
                if (divTest(x, y) != functionResult) {
                    return;
                }
            }
            System.out.println();
        }
    }

    private static int divTest(int x, int y) {
        return (x / y);
    }

    private static PrimitiveRecursiveFunction remainder() throws PrimitiveRecursionException {
        PrimitiveRecursiveFunction not = AdvancedFunctions.getWrapperFunction(AdvancedFunctions.not, "not");
        PrimitiveRecursiveFunction eq = AdvancedFunctions.getWrapperFunction(AdvancedFunctions.equalityTest, "test_{=}");
        PrimitiveRecursiveFunction vg = AdvancedFunctions.decrement;

        PrimitiveRecursiveFunction pi3to1 = AdvancedFunctions.pi3to1;
        PrimitiveRecursiveFunction pi3to3 = AdvancedFunctions.pi3to3;

        PrimitiveRecursiveFunction ifSwitch = new PrimitiveRecursion(new Constant(1, 0), new Composition(new Increment(), pi3to3));
        PrimitiveRecursiveFunction testValue = new Composition(new Composition(not, eq), pi3to1, new Composition(vg, pi3to3));
        PrimitiveRecursiveFunction recursiveFunction = new Composition(ifSwitch, testValue, pi3to1);

        return new PrimitiveRecursion(new Constant(1, 0), recursiveFunction);
    }

    private static PrimitiveRecursiveFunction h82(PrimitiveRecursiveFunction remainder) throws PrimitiveRecursionException {
        PrimitiveRecursiveFunction not = AdvancedFunctions.getWrapperFunction(AdvancedFunctions.not, "not");
        PrimitiveRecursiveFunction eq = AdvancedFunctions.getWrapperFunction(AdvancedFunctions.equalityTest, "test_{=}");
        PrimitiveRecursiveFunction vg = AdvancedFunctions.decrement;

        PrimitiveRecursiveFunction pi1to1 = AdvancedFunctions.pi1to1;
        PrimitiveRecursiveFunction pi3to1 = AdvancedFunctions.pi3to1;
        PrimitiveRecursiveFunction pi3to2 = AdvancedFunctions.pi3to2;
        PrimitiveRecursiveFunction pi3to3 = AdvancedFunctions.pi3to3;

        PrimitiveRecursiveFunction ifSwitch = new PrimitiveRecursion(new Composition(new Increment(), pi1to1), pi3to3);
        PrimitiveRecursiveFunction testValue = new Composition(new Composition(not, eq), new Composition(remainder, pi3to2, pi3to3), new Composition(vg, pi3to3));
        PrimitiveRecursiveFunction recursiveFunction = new Composition(ifSwitch, testValue, pi3to1);

        return new PrimitiveRecursion(new Constant(1, 0), recursiveFunction);
    }

    private static PrimitiveRecursiveFunction minimised(PrimitiveRecursiveFunction primitive) throws PrimitiveRecursionException {
        PrimitiveRecursiveFunction not = AdvancedFunctions.getWrapperFunction(AdvancedFunctions.not, "not");
        PrimitiveRecursiveFunction eq = AdvancedFunctions.getWrapperFunction(AdvancedFunctions.equalityTest, "test_{=}");

        PrimitiveRecursiveFunction pi3to1 = AdvancedFunctions.pi3to1;
        PrimitiveRecursiveFunction pi3to2 = AdvancedFunctions.pi3to2;
        PrimitiveRecursiveFunction pi3to3 = AdvancedFunctions.pi3to3;
        PrimitiveRecursiveFunction pi5to3 = new Projection(5, 3);
        PrimitiveRecursiveFunction pi5to4 = new Projection(5, 4);
        PrimitiveRecursiveFunction pi5to5 = new Projection(5, 5);

        PrimitiveRecursiveFunction tester = new Composition(new Composition(not, eq), pi5to3, new Composition(primitive, pi5to4, pi5to5));

        PrimitiveRecursiveFunction wrapper = new Composition(new PrimitiveRecursion(new Constant(3, 1), tester), pi3to3, pi3to1, pi3to2, pi3to3);
        System.out.println(wrapper.generateLatexRepresentation());
        return new Minimisation(wrapper);
    }
}
