private static long fibLoopStrict(long x1, long x2, long x3, long x4, long x5) {
        //if x1 == 0 then ... else ...
        x4 = 1; //LOOP(x4){x4 = x4 - 1}; x4 = x4 + 1;
        x5 = 0; //LOOP(x5){x5 = x5 - 1};
        for (long n = x1; n > 0; n--) { x4 = 0; x5 = 1;}
        for (long n = x4; n > 0; n--) { x1 = x1 + 0;}
        for (long n1 = x5; n1 > 0; n1--) {
            //if x1 == 1 then ... else ...
            x1 = x1 - 1;
            x4 = 1; //LOOP(x4){x4 = x4 - 1}; x4 = x4 + 1;
            x5 = 0; //LOOP(x5){x5 = x5 - 1};
            for (long n = x1; n > 0; n--) { x4 = 0; x5 = 1;}
            for (long n = x4; n > 0; n--) { x1 = x1 + 1;}
            for (long n4 = x5; n4 > 0; n4--) {
                //x1 is >= 2
                x2 = 0; //LOOP(x2){x2 = x2 - 1}; x2 = x2 + 1;
                x3 = 1; //LOOP(x3){x3 = x3 - 1}

                //LOOP(x1)
                for (long n3 = x1; n3 > 0; n3--) {
                    //x1 = x2 + x3;
                    x1 = x2 + 0;
                    for (long n = x3; n > 0; n--) { x1 = x1 +1;}

                    x2 = x3 + 0;
                    x3 = x1 + 0;
                }
            }
        }

        return x1;
    }
