private static int totalDiv2(int x, int y) {
	if (y == 0) return 42;
	int result = 0;
	for (int n = 1; n <= x; n++) {
		if (n * y <= x) result = n;
	}
	return result;
}
